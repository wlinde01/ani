﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace Ani.Stats.Playground.WebApi.Controllers
{
    [RoutePrefix("api/pain")]
    public class PainController : ApiController
    {
        [Route(""), HttpGet]
        public string Index()
        {
            var stats = Request.GetOwinEnvironment()[AniExtensions.OperationNameKey].ToString();
            return stats;
        }

        [Route("vouchergroup/{id}/voucher/{voucherId}"), HttpGet]
        public string GetVoucherGroupVoucher(string id, string voucherId)
        {
            // ExtractAniMetricsControllerRouteBased used from global filter in WebApiConfig

            var stats = Request.GetOwinEnvironment()[AniExtensions.OperationNameKey].ToString();
            return stats;
        }

        [ExtractAniMetricsControllerRouteBased(ExcludeFromRoute = new[] { "{id}", "{voucherId}", "kiss" })]
        [Route("vouchergroup/{id}/voucher/{voucherId}/kiss"), HttpGet]
        public string GetVoucherGroupVoucherCrap(string id, string voucherId)
        {
            var stats = Request.GetOwinEnvironment()[AniExtensions.OperationNameKey].ToString();
            return stats;
        }
    }
}
