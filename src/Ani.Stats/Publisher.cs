﻿using System;
using JustEat.StatsD;


namespace Ani.Stats
{

    internal class AniPublisher
    {
        private readonly IStatsDPublisher _statsDPublisher;

        internal AniPublisher(IStatsDPublisher statsDPublisher)
        {
            _statsDPublisher = statsDPublisher;
        }

        internal void Publish(string bucket, TimeSpan? responseTime = null)
        {

            _statsDPublisher.Increment(bucket);

            if (responseTime != null)
                _statsDPublisher.Timing((TimeSpan) responseTime, bucket);
        }
    }
}
