using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Ani.Stats
{
    public class AniConfig
    {

        // ToDo: Add configurable "disable" default bucket naming. No default bucket prefix (ioc inserted stats publisher might not want any prefix / medling with bucket naming)

        public bool ExtractHttpMethod { get; private set; }
        public AniConfig WithExtractHttpMethod()
        {
            ExtractHttpMethod = true;
            return this;
        }

        
        public bool TimeRequests { get; private set; }
        public AniConfig WithTimeRequests()
        {
            TimeRequests = true;
            return this;
        }


        public string Prefix { get; private set; }
        public AniConfig WithPrefix(string prefix)
        {
            Prefix = prefix;
            return this;
        }

        public bool ExtractHttpResponseCode { get; private set; }
        public AniConfig WithExtractHttpResponseCode()
        {
            ExtractHttpResponseCode = true;
            return this;
        }

        public List<string> HeaderKeys { get; private set; }
        public AniConfig WithHeaderKeys(List<string> headerKeys)
        {
            HeaderKeys = headerKeys;
            return this;
        }

        public AniConfig WithHeaderKey(string headerKey)
        {
            if (HeaderKeys == null) HeaderKeys = new List<string>();
            HeaderKeys.Add(headerKey);
            return this;
        }

        public bool WithUrlExtraction { get; private set; }
        public Regex UrlRegEx { get; private set; }
        public AniConfig WithRequestUrlExtraction(string urlRegEx)
        {
            WithUrlExtraction = true;
            UrlRegEx = new Regex(urlRegEx, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return this;
        }

        public bool ExtractOperation { get; private set; }
        public string OperationKey { get; private set; }
        public AniConfig WithOperationExtraction(string operationNameKey = AniExtensions.OperationNameKey)
        {
            ExtractOperation = true;
            OperationKey = operationNameKey;
            return this;
        }
    }
}