using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using JustEat.StatsD;
using Owin;
using static Ani.Stats.AniExtensions;

namespace Ani.Stats
{


    public static class AniAppBuilderExtensions
    {
        public static IAppBuilder UseAniMiddleware(this IAppBuilder builder, IStatsDPublisher statsDPublisher)
        {
            return builder.Use(typeof(StatsDMiddleware), new AniPublisher(statsDPublisher));
        }

        public static IAppBuilder UseAniMiddleware(this IAppBuilder builder, IStatsDPublisher statsDPublisher, AniConfig aniConfig) // Should not use Ani.AniPublisher, use IStatsDPublisher (??)
        {
            return builder.Use(typeof(StatsDMiddleware), new AniPublisher(statsDPublisher), aniConfig);
        }
    }


    //ToDo Tests need fixing to reflect working with OWIN 
    public class ExtractAniMetricsControllerActionBased : ActionFilterAttribute
    {
        public string ControllerOverride { get; set; }
        public string ActionOverride { get; set; }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var controller = actionContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var action = actionContext.ActionDescriptor.ActionName;
            var operation = $"{ControllerOverride ?? controller}.{ActionOverride ?? action}";

            var env = actionContext.Request.GetOwinEnvironment();
            env.Add(AniExtensions.OperationNameKey, operation); // Drop Hardcoded default OperationKey
        }
    }

    public class ExtractAniMetricsControllerRouteBased : ActionFilterAttribute
    {
        public string RegexOverride { get; set; }
        public string RouteOverride { get; set; }
        public string[] ExcludeFromRoute { get; set; }
        public override bool AllowMultiple => false;

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var routeStatsBucket = string.Empty;
            var inCommingRoute = actionContext.Request.RequestUri.LocalPath;
            if (RouteOverride?.Length > 0)
            {
                routeStatsBucket = RouteOverride;
            }
            else if (ExcludeFromRoute?.Length > 0)
            {
                var statPoints = inCommingRoute.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                var routeData = actionContext.RequestContext.RouteData;
                var routeTemplatePoints = routeData.Route.RouteTemplate.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                var stats = new StringBuilder();
                for (var i = 0; i < statPoints.Length; i++)
                {
                    if (ExcludeFromRoute.All(e => !e.Equals(routeTemplatePoints[i], StringComparison.CurrentCultureIgnoreCase)))
                    {
                        stats.Append(statPoints[i]);
                        stats.Append("/");
                    }
                }

                routeStatsBucket = stats.ToString();
            }
            else if (IsValidRegex(RegexOverride))
            {
                var regEx = new Regex(RegexOverride, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                var groups = regEx.Match(inCommingRoute).Groups;
                for (var i = 1; i < groups.Count; i++)
                {
                    routeStatsBucket = ConcatBucketBits(routeStatsBucket, groups[i].Value);
                }
            }
            else routeStatsBucket = inCommingRoute;

            routeStatsBucket = routeStatsBucket.TrimStart('/').TrimEnd('/');
            routeStatsBucket = routeStatsBucket.Equals(string.Empty) ? inCommingRoute.Replace("/", ".") : routeStatsBucket.Replace("/", ".");
            var env = actionContext.Request.GetOwinEnvironment();
            env.Add(AniExtensions.OperationNameKey, routeStatsBucket);
        }
    }


    //ToDo Borken: does not seem to put the data into the header early enough or at all, needs debugging
    /*public class RequestOperationToHeaderActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var env = actionContext.Request.GetOwinEnvironment();
            var controller = actionContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var action = actionContext.ActionDescriptor.ActionName;

            string operationName = $"{controller}-{action}";
            actionContext.Request.Headers.Add(AniExtensions.OperationNameKey, operationName);
        }
    }*/


    /*    public class AniStatsExceptionFilter : ExceptionFilterAttribute
        {
            private IStatsDPublisher StatsDPublisher { get; set; }

            public AniStatsExceptionFilter(IStatsDPublisher statsDPublisher)
            {
                this.StatsDPublisher = statsDPublisher;
            }

            public string ControllerOverride { get; set; }
            public string ActionOverride { get; set; }
            public override void OnException(HttpActionExecutedContext executedContext)
            {
                //var statsDPublisher  = executedContext.Request.GetDependencyScope().GetService(typeof(IStatsDPublisher)) as StatsDImmediatePublisher;

                if (StatsDPublisher != null)
                {
                    var controller = executedContext.ActionContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                    var action = executedContext.ActionContext.ActionDescriptor.ActionName;

                    var operation = $"{ControllerOverride ?? controller}.{ActionOverride ?? action}";
                    var exceptionName = executedContext.Exception.GetType().Name;
                    StatsDPublisher.Increment($"{operation}.exceptions.{exceptionName}");
                }

                base.OnException(executedContext);
            }
        }*/
}