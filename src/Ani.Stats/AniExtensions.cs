﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using JustEat.StatsD;

namespace Ani.Stats
{
    public static class AniExtensions
    {
        public const string DefaultBucketName = "ani-default";
        public const string OperationNameKey = "ani-operation-name";
        private static readonly Regex PunctuationRegex = new Regex("[^a-zA-Z0-9.-]", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public static string ConcatBucketBits(string thing, string otherThing) => (thing == null) ? otherThing : $"{thing}.{otherThing}";
        public static string CleanBucketName(string bucket)
        {
            return bucket == null ? null : PunctuationRegex.Replace(bucket, "").ToLowerInvariant();
        }


        public static bool IsValidRegex(string pattern)
        {
            if (string.IsNullOrEmpty(pattern)) return false;

            try
            {
                Regex.Match("", pattern);
            }
            catch (ArgumentException)
            {
                return false;
            }

            return true;
        }

        //ToDo: Any usefull Canned regex's ?? >>
        //public static readonly Regex AniUriLast = new Regex(@".*/(.*)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        //public static readonly Regex AniUriFirst = new Regex(@".*/(.*)/.*$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        //ToDo: consider using flags instead for pulling out Uri properties directly
        //public const string AniUriHost = @"^http://([0-9a-zA-Z-.]*)/.*"; // = new Regex(@"^(.*)/.*", RegexOptions.Compiled | RegexOptions.IgnoreCase);


    }
}
