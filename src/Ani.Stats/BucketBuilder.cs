using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using static Ani.Stats.AniExtensions;


namespace Ani.Stats
{
    public class BucketBuilder
    {
        private readonly AniConfig _aniConfig ;
        public BucketBuilder(AniConfig aniConfig)
        {
            _aniConfig = aniConfig;
        }

        private string GenerateBucket(string httpMethod, int statusCode, List<string> headerValues, string uri, string operation)
        {
            var bucket = _aniConfig.Prefix??DefaultBucketName;

            if (_aniConfig.WithUrlExtraction && _aniConfig.UrlRegEx != null)
            {
                var groups = _aniConfig.UrlRegEx.Match(uri).Groups;
                for (var i = 1; i < groups.Count; i++)
                {
                    bucket = ConcatBucketBits(bucket, groups[i].Value);
                }
            }

            if (_aniConfig.ExtractOperation && operation != null)
            {
                bucket = ConcatBucketBits(bucket, operation);
            }

            bucket = _aniConfig.ExtractHttpMethod ? ConcatBucketBits(bucket, httpMethod) : bucket;
            bucket = _aniConfig.ExtractHttpResponseCode ? ConcatBucketBits(bucket, statusCode.ToString()) : bucket;
            if (headerValues != null) bucket = headerValues.Aggregate(bucket, ConcatBucketBits);

            return CleanBucketName(bucket);
        }

        


        public string GenerateBucket(IOwinContext context, int? statusCodeOverride = null)
        {
            var httpMethod = context.Request.Method;
            var statusCode = statusCodeOverride??context.Response.StatusCode;
            var uri = context.Request.Uri.AbsolutePath;
            //var scheme = context.Request.Uri.Scheme;
            //var resource = context.Request.Uri.LocalPath;

            List<string> headerValues = null; //ToDo dont 'use' null ?? 
            if (_aniConfig?.HeaderKeys != null) headerValues = _aniConfig.HeaderKeys.Select(key => context.Request.Headers[key]).ToList();

            string operation = null; //ToDo: Use nulls??
            if (_aniConfig != null && _aniConfig.ExtractOperation)
            {
                //ToDo: Make sure it is safe to use when context.Environment is not available // Seems its not - confirm etc..

                var operationInContext = context.Environment.ContainsKey(_aniConfig.OperationKey);
                var operationInHeader = context.Request.Headers.ContainsKey(_aniConfig.OperationKey);

                if (operationInHeader) operation = context.Request.Headers[_aniConfig.OperationKey];
                else if (operationInContext) operation = context.Environment[_aniConfig.OperationKey] as string;
                else operation = "operation-unknown";

/*                operation = context.Request.Headers.ContainsKey(_aniConfig.OperationKey)
                    ? context.Request.Headers[_aniConfig.OperationKey]
                    : context.Environment[_aniConfig.OperationKey] as string;*/
            }


            var bucket = GenerateBucket(httpMethod, statusCode, headerValues, uri, operation);
            return bucket;
        }
    }
}