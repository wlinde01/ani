﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Owin;

namespace Ani.Stats
{
    class StatsDMiddleware : OwinMiddleware
    {
        private readonly AniPublisher _aniPublisher;
        private readonly BucketBuilder _bucketBuilder;
        private readonly bool _timeRequests;


        //ToDo: headerkeys and timing probably MiddleWare specifics - move them out of conf into here
        //ToDo: Add contextKeys / how to make / use them SAFE

        public StatsDMiddleware(OwinMiddleware next, AniPublisher aniPublisher) : base(next)
        {
            _aniPublisher = aniPublisher;
            _timeRequests = false;
            _bucketBuilder = new BucketBuilder(new AniConfig());
        }

        public StatsDMiddleware(OwinMiddleware next, AniPublisher aniPublisher, AniConfig aniConfig) : this(next, aniPublisher) 
        {
            _timeRequests = aniConfig.TimeRequests;
            _bucketBuilder = new BucketBuilder(aniConfig);
        }


        public override async Task Invoke(IOwinContext context)
        {

            var excepted = false;
            var responseTime = Stopwatch.StartNew();

            try
            {
                await Next.Invoke(context);
            }
            catch
            {
                excepted = true;
                throw;
            }
            finally
            {
                var statusCode = excepted ? 500 : context.Response.StatusCode;
                var bucket = _bucketBuilder.GenerateBucket(context, statusCode);

                // Only publish timing if requested
                if (_timeRequests) _aniPublisher.Publish(bucket, responseTime.Elapsed);
                else _aniPublisher.Publish(bucket);
            }
        }
    }
}
