﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using FakeItEasy;
using FakeItEasy.ExtensionSyntax.Full;
using JustEat.StatsD;
using Microsoft.Owin.Testing;
using NUnit.Framework;
using Ani.Stats;
using FakeItEasy.ExtensionSyntax;

namespace Tests.Unit
{
    [TestFixture]
    public class WhenUsingUrlPatternExtraction
    {
        private TestServer _testServer;
        private const string BucketPrefix = "ani-prefix";

        [SetUp]
        public void SetUp()
        {
/*            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_publisherToTest);
            });*/

        }


        [Test]
        public void PublishesRegexPaternFromRequestUrlInBucketName()
        {

            var fakeStatsD = A.Fake<IStatsDPublisher>();
            var urlRegEx = @".*/(.*)$";

            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(fakeStatsD,
                    new AniConfig()
                    .WithPrefix(BucketPrefix)
                    .WithRequestUrlExtraction(urlRegEx)
                    );
            });

            var response = _testServer
                .CreateRequest("http://someurl/someaction/subaction?s=2")
                .GetAsync().Result;

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.subaction")))
            .MustHaveHappened();

        }

        [Test]
        public void PublishesRegexPaternFromUrlWithMultipleGroups()
        {

            var fakeStatsD = A.Fake<IStatsDPublisher>();
            var urlRegEx = @".*/(.*)/(.*)$";

            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(fakeStatsD,
                    new AniConfig()
                    .WithPrefix(BucketPrefix)
                    .WithRequestUrlExtraction(urlRegEx)
                    );
            });

            var response = _testServer
                .CreateRequest("http://baseurl/someaction/subaction?s=2")
                .GetAsync().Result;

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.someaction.subaction")))
            .MustHaveHappened();

        }

        [Test]
        public void IgnoresRegexThatMatchesNothing()
        {

            var fakeStatsD = A.Fake<IStatsDPublisher>();
            var urlRegEx = @".*/(.*)/(.*)/(.*)$";

            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(fakeStatsD,
                    new AniConfig()
                    .WithPrefix(BucketPrefix)
                    .WithRequestUrlExtraction(urlRegEx)
                    );
            });

            var response = _testServer
                .CreateRequest("http://baseurl/someaction/subaction?s=2")
                .GetAsync().Result;

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}")))
            .MustHaveHappened();

        }

        // Ignored      
        public void MatchesOnAniUriHost()
        {
            var fakeStatsD = A.Fake<IStatsDPublisher>();

            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(fakeStatsD,
                    new AniConfig()
                    .WithPrefix(BucketPrefix)
                   // .WithRequestUrlExtraction(AniExtensions.AniUriHost)
                    );
            });

            var response = _testServer
                .CreateRequest("http://host/someaction/subaction?s=2")
                .GetAsync().Result;

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.host")))
            .MustHaveHappened();

        }

    }
}
