﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using FakeItEasy;
using FakeItEasy.ExtensionSyntax.Full;
using JustEat.StatsD;
using Microsoft.Owin.Testing;
using NUnit.Framework;
using Ani.Stats;
using FakeItEasy.ExtensionSyntax;
using Owin;

namespace Tests.Unit
{
   
    [TestFixture]
    public class WhenUsingOwin
    {

        // ToDo: Make this consistent... share vs test run specific testserver / _facekStatsD 
        private TestServer _testServer;
        private readonly IStatsDPublisher _fakeStatsD = A.Fake<IStatsDPublisher>();

        //_publisher = new StatsDImmediatePublisher(CultureInfo.InvariantCulture, "the_host");
        //private HttpResponseMessage _response;
        
        private const string BucketPrefix = "ani-prefix";

        [SetUp]
        public void SetUp()
        {
            //ToDo: Abstract a bit better to avoid current state  dependencies to following tests
            

            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD);
            });

        }


        [Test]
        public void ExceptionIsBubbledAndStatsAs500()
        {
            var statsDPublisher = A.Fake<IStatsDPublisher>();

            // Create an exception throwing server
            var testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(statsDPublisher, new AniConfig()
                    .WithTimeRequests()
                    .WithExtractHttpResponseCode()
                    .WithExtractHttpMethod());

                builder.Use((env, next) =>
                {
                    throw new Exception();
                });
            });


            Assert.ThrowsAsync<Exception>(async () => await testServer.CreateRequest("http://someurl/method1").GetAsync());
            statsDPublisher.CallsTo(
                x =>
                    x.Timing(
                        A<TimeSpan>.That.Matches(y => y != null),
                        A<string>.That.Matches(y => y == $"{AniExtensions.DefaultBucketName}.get.500"))).MustHaveHappened();

            statsDPublisher.CallsTo(
                x =>
                    x.Increment(
                        FakeItEasy.A<string>.That.Matches(y => y == $"{AniExtensions.DefaultBucketName}.get.500"))).MustHaveHappened();
        }

        [Test]
        public void WithNoConfiguration()
        {
            var response = _testServer.CreateRequest("http://someurl/somemethod").GetAsync().Result;
            _fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == AniExtensions.DefaultBucketName)))
                .MustHaveHappened();
        }

        [Test]
        public void PublishesBadCharactersInBucketNameSafely()
        {
            var _fakeStatsD = A.Fake<IStatsDPublisher>();
            


            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD,
                    new AniConfig()
                    .WithHeaderKey("badcharacterkey"));
            });


            var response = _testServer.CreateRequest("http://someurl/somemethod")
                .AddHeader("badcharacterkey", "bad%character")
                .GetAsync().Result;

            _fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{AniExtensions.DefaultBucketName}.badcharacter")))
                .MustHaveHappened();
        }

        [Test]
        public void RequestIsTimed()
        {

            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD,
                    new AniConfig()
                    .WithTimeRequests());
            });


            var response = _testServer.CreateRequest("http://someurl/somemethod").GetAsync().Result;

            _fakeStatsD.CallsTo(
                   x =>
                       x.Timing(
                           A<TimeSpan>.That.Matches(y => y > TimeSpan.FromMilliseconds(0.000001)),
                           A<string>.That.Matches(y => y == AniExtensions.DefaultBucketName)))
                       .MustHaveHappened();
        }

        [Test]
        public void RequestWithPrefixIsNotTimed()
        {
            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD,
                    new AniConfig()
                    .WithPrefix("notiming"));

            });


            var response = _testServer.CreateRequest("http://someurl/somemethod").GetAsync().Result;

            _fakeStatsD.CallsTo(
                   x =>
                       x.Timing(
                           A<TimeSpan>.That.Matches(y => y > TimeSpan.FromMilliseconds(0.001)),
                           A<string>.That.Matches(y => y == "notiming")))
                       .MustNotHaveHappened();

            _fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == "notiming")))
                .MustHaveHappened();

        }

        [Test]
        public void RequestWithPrefixIsTimed()
        {
            //ToDo: Intermitently fails with "This test was not executed during a planned execution run. << Seemed to have stopped?"

            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD,
                    new AniConfig()
                    .WithTimeRequests()
                    .WithPrefix(BucketPrefix));

            });

            var response = _testServer.CreateRequest("http://someurl/somemethod").GetAsync().Result;

            var expectedBucketName = $"{BucketPrefix}";

            _fakeStatsD.CallsTo(
                   x =>
                       x.Timing(
                           //ToDo: Introduce forced response delay and confirm timing stat corresponds
                           A<TimeSpan>.That.Matches(y => y > TimeSpan.FromMilliseconds(0.00000001)),
                           A<string>.That.Matches(y => y == expectedBucketName)))
                       .MustHaveHappened();
        }


        [Test]
        public void PublishesHttpMethod()
        {
            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD, new AniConfig().WithExtractHttpMethod());
            });

            var response = _testServer.CreateRequest("http://someurl/somemethod").GetAsync().Result;

            // why is this not getting default bucket name?? (Because default fallback happens in publisher....)
            _fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{AniExtensions.DefaultBucketName}.get")))
                .MustHaveHappened();
        }


        [Test]
        [Ignore("Test needs tweaking, add owin context into test")]
        public void PublishesHttpResponseCode()
        {
            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD, 
                    new AniConfig()
                    .WithExtractHttpResponseCode()
                    .WithPrefix("response"));
            });

            var response = _testServer.CreateRequest("http://someurl/somemethod").GetAsync().Result;

            _fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == "response.200")))
                .MustHaveHappened();
        }



        [Test]
        public void PublishesHttpMethodWithPrefix()
        {
            //ToDo: Why cant I break this test : throws exception but ncrunch shows pass
            var _fakeStatsD = A.Fake<IStatsDPublisher>();
            
            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD,
                    new AniConfig()
                    .WithExtractHttpMethod()
                    .WithPrefix(BucketPrefix)
                    );
            });

            var response = _testServer.CreateRequest("http://someurl/somemethod").GetAsync().Result;

            _fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.get")))
                .MustHaveHappened();
        }


        [Test]
        public void PublishesSingleHeaderValue()
        {

            const string headerKey = "MyCustomHeaderKey";
            const string headerValue = "customheadervalue";

            var _fakeStatsD = A.Fake<IStatsDPublisher>();
            
            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD,
                    new AniConfig()
                    .WithHeaderKey(headerKey)
                    .WithPrefix(BucketPrefix)
                    );
            });

            var response = _testServer.CreateRequest("http://someurl/somemethod")
                .AddHeader(headerKey, headerValue)
                .GetAsync().Result;

            _fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.{headerValue}")))
            .MustHaveHappened();
        }


        [Test]
        public void PublishesMultipleHeaderValuesInOrderOfListOfHeaderKeys()
        {
            var headerKeys = new List<string> {"MyCustomHeader1", "MyCustomHeader2"};
            var headerValues = new List<string> { "beta-traffic", "perf-load" };

            var _fakeStatsD = A.Fake<IStatsDPublisher>();
            

            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(_fakeStatsD,
                    new AniConfig()
                    .WithHeaderKeys(headerKeys)
                    .WithPrefix(BucketPrefix)
                    );
            });

            var response = _testServer.CreateRequest("http://someurl/somemethod")
                .AddHeader(headerKeys[0], headerValues[0])
                .AddHeader(headerKeys[1], headerValues[1])
                .GetAsync().Result;

            _fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.{headerValues[0]}.{headerValues[1]}")))
            .MustHaveHappened();
        }

    }
}
