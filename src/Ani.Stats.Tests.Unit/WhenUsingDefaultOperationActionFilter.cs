﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ani.Stats;
using FakeItEasy;
using FakeItEasy.ExtensionSyntax.Full;
using JustEat.StatsD;
using Microsoft.Owin.Testing;
using NUnit.Framework;
using Owin;

namespace Tests.Unit
{
    internal class WhenUsingDefaultOperationActionFilter
    {

        private TestServer _testServer;
        private const string BucketPrefix = "ani-prefix";


        [Test]
        public void PublishesOperationActionFilterFromOwinEnvironment()
        {
            var fakeStatsD = A.Fake<IStatsDPublisher>();
            _testServer = TestServer.Create(builder =>
            {
                builder.Use(async (env, next) =>
                {
                    env.Environment.Add(AniExtensions.OperationNameKey, env.Request.Path.Value.TrimStart('/'));
                    await next();
                });

                builder.UseAniMiddleware(fakeStatsD,
                    new AniConfig()
                    .WithPrefix(BucketPrefix)
                    .WithOperationExtraction(AniExtensions.OperationNameKey));

            });

            var httpResponseMessage = _testServer.CreateRequest("http://someurl/method1").GetAsync().Result;

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.method1")))
                .MustHaveHappened();
        }


        [Test]
        public void PublishesControllerActionFromOwinEnvironment()
        {
            var fakeStatsD = A.Fake<IStatsDPublisher>();

            _testServer = TestServer.Create(builder =>
            {
                builder.Use(async (env, next) =>
                {
                    env.Environment.Add(AniExtensions.OperationNameKey, env.Request.Path.Value.TrimStart('/').Replace('/', '-'));
                    await next();
                });

                builder.UseAniMiddleware(fakeStatsD,
                    new AniConfig()
                    .WithPrefix(BucketPrefix)
                    .WithOperationExtraction(AniExtensions.OperationNameKey));

            });

            var httpResponseMessage = _testServer.CreateRequest("http://someurl/controller/action").GetAsync().Result;

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.controller-action")))
                .MustHaveHappened();
        }

        [Test]
        public void DoesNotPublishControllerActionFromOwinEnvironment()
        {
            var fakeStatsD = A.Fake<IStatsDPublisher>();

            _testServer = TestServer.Create(builder =>
            {
                builder.Use(async (env, next) =>
                {
                    env.Environment.Add(AniExtensions.OperationNameKey, env.Request.Path.Value.TrimStart('/').Replace('/', '-'));
                    await next();
                });

                builder.UseAniMiddleware(fakeStatsD,
                    new AniConfig()
                    .WithPrefix(BucketPrefix));

            });

            var httpResponseMessage = _testServer.CreateRequest("http://someurl/controller/action").GetAsync().Result;

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.controller-action")))
                .MustNotHaveHappened();

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}")))
                .MustHaveHappened();
        }

        [Test]
        public void PublishesControllerActionFromRequestHeader()
        {
            var fakeStatsD = A.Fake<IStatsDPublisher>();
            _testServer = TestServer.Create(builder =>
            {
                builder.Use(async (env, next) =>
                {
                    env.Request.Headers.Append(AniExtensions.OperationNameKey, env.Request.Path.Value.TrimStart('/').Replace('/', '-'));
                    await next();
                });

                builder.UseAniMiddleware(fakeStatsD,
                    new AniConfig()
                    .WithPrefix(BucketPrefix)
                    .WithOperationExtraction(AniExtensions.OperationNameKey));

            });

            var httpResponseMessage = _testServer.CreateRequest("http://someurl/controller/action").GetAsync().Result;

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.controller-action")))
                .MustHaveHappened();
        }

    }
}