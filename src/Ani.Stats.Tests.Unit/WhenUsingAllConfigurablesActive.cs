﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using FakeItEasy;
using FakeItEasy.ExtensionSyntax.Full;
using JustEat.StatsD;
using Microsoft.Owin.Testing;
using NUnit.Framework;
using Ani.Stats;
using FakeItEasy.ExtensionSyntax;

namespace Tests.Unit
{
    [TestFixture]
    [Ignore("Test needs tweaking, add owin context into test")]
    public class WhenUsingAllConfigurablesActive
    {
        private TestServer _testServer;
        private const string BucketPrefix = "ani-prefix";


        [Test]
        public void PublishesFullConfigInTheCorrectOrder()
        {

            var fakeStatsD = A.Fake<IStatsDPublisher>();
            var urlRegEx = @".*/(.*)/.*$";

            _testServer = TestServer.Create(builder =>
            {
                builder.UseAniMiddleware(fakeStatsD,
                    new AniConfig()
                        .WithPrefix(BucketPrefix)
                        .WithRequestUrlExtraction(urlRegEx)
                        .WithExtractHttpResponseCode()
                        .WithExtractHttpMethod()
                        .WithHeaderKey("header-suffix-key")
                        .WithOperationExtraction("operation-key")
                        .WithTimeRequests()
                        
                    );
            });

            var response = _testServer
                .CreateRequest("http://someurl/controllerurl/actionurl?s=2")
                .AddHeader("header-suffix-key","header-suffix")
                .AddHeader("operation-key","an-operation.some-action")
                .GetAsync().Result;

            fakeStatsD.CallsTo(x => x.Increment(A<string>.That.Matches(y => y == $"{BucketPrefix}.controllerurl.an-operation.some-action.get.200.header-suffix")))
                .MustHaveHappened();

            fakeStatsD.CallsTo(
                x => x.Timing(
                       A<TimeSpan>.That.Matches(y => y > TimeSpan.FromMilliseconds(0.000001)),
                       A<string>.That.Matches(y => y == $"{BucketPrefix}.controllerurl.an-operation.some-action.get.200.header-suffix")))
                   .MustHaveHappened();

        }
    }
}
