# README #


### What is this repository for? ###

* .Net utility to produce statsd metrics for .Net web applications.

### ToDos

* Introduce optional logging
* Add "priority" to AniConfig / bucket element properties. Build the bucket name in the order of properties added.
* Check which characters are valid graphite characters.
* Introduce "safe" high level exception handling
* Don't use null to flag meaning; only use null 'tolerance' for externally provided datums (??)
* Refactor nasty tests
* Start documentation
* Bucket generation from list of functions.
* Make Publisher IDisposable

### Using Ani ###

#### Dependencies ####
je.statsd; owin; webapi; 


#### Startup ####
~~~~            
using Ani.Stats;
using Owin;
using JustEat.StatsD;

/****/

public partial class Startup
{
        public void Configuration(IAppBuilder app)
        {

            const string statsDPrefix = "application-name"; 
            const string statsDHostname = "statsd-hostname.com";
            var publisher = new StatsDImmediatePublisher(CultureInfo.CurrentCulture, statsDHostname, prefix: statsDPrefix);

            var aniConfig = new AniConfig()
                
                // Produces *.application-name.api-name
                .WithPrefix("api-name")

                // Produces *.application-name.api-name.controller.action
                .WithOperationExtraction() // Hooking into Controller Action

                // Produces *.application-name.api-name.controller.action.post
                .WithExtractHttpMethod()

                // Produces *.application-name.api-name.controller.action.post.200
                .WithExtractHttpResponseCode()

                // Produces same bucket naming as constructed, including response timers
                .WithTimeRequests();
            
            // From Ani.Stats.AniAppBuilderExtensions
            app.UseAniMiddleware(publisher, aniConfig);
        }
    }

~~~~


#### Hooking into Controller Action  ####
~~~~
using System.Reflection;
using System.Web.Http;
using Ani.Stats;

namespace Application.Api.Controllers
{

    public class HealthController : ApiController
    {
        [HttpGet]
        [Route("health/check")]
        [Route("api/health")]
        [ExtractControllerNameAndAction(ControllerOverride = "monitoring", ActionOverride = "healthcheck")]
        // Produces *.application-name.api-name.monitoring.healthcheck.get.[200|500|etc]
        public IHttpActionResult Check()
        {
            return Ok(Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }

        [HttpGet]
        [Route("health/validate")]
        [ExtractControllerNameAndAction(ActionOverride = "validation")]
        // Produces *.application-name.api-name.health.validation.get.[200|500|etc]
        public IHttpActionResult Validate()
        {
            return Ok(Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }
    }
}

~~~~

#### More examples, using ANI's implemented action filters ####

[1] Add the std Controller-Action based filter:
~~~~
config.Filters.Add(new ExtractAniMetricsControllerActionBased());
~~~~
Adds controller name and action (method) name to generated statsd bucket.

[2] Ignores any RouteParameter named {id}
~~~~
config.Filters.Add(new ExtractAniMetricsControllerRouteBased { ExcludeFromRoute = new[] { "{id}" } });
~~~~

[3] Use Regex to define groups used for statsd bucket generation (only matches contained within groups are used for bucket generation)
~~~~
[ExtractAniMetricsControllerRouteBased(RegexOverride = @"(.*)/.*/(.*)$")]
[Route("{country}/email/{id}/list")]
~~~~
Has the same affect as example #2, but makes use of regex and used as attribute against a specific controller/action.